let bookindex = 10
let query;

$.get("https://random-word-api.herokuapp.com/word?number=1", (res) => {
    $.get(`/v8/api?q=${res[0]}`, (init) => {
        let books = init.items;
        query = res[0]
        if (books)
            books.forEach((volume) => {
                display(volume);
            });
    })
})

$("#query").keyup((e) => {
    let $this = $(e.target);

    $.ajax({
        url: `/v8/api?q=${$this.val()}`,
        dataType: "json",

        success: (response) => {
            // Returns 10 books each call

            bookindex = 10
            query = $this.val()

            if (response.items) {
                $(".content-wrapper").empty();
                response.items.forEach((book) => {
                    display(book)
                });
            };
        },

        error: (response) => {
            console.log(response)
        }
    });
});

$(".jumbotron > button").click((e) => { 
    e.preventDefault();
    
    $.get(`/v8/api?q=${query}&startIndex=${bookindex}`, (init) => {
        let books = init.items;

        if (books)
            $(".content-wrapper").empty();
            books.forEach((volume) => {
                display(volume);
            });
        
        bookindex += 10
    })

    document.body.scrollTop = 0;            // For Safari
    document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera

});

const display = (books) => {
    // let isbn10 = (books.industryIdentifiers[0].type == "ISBN_10") ? books.industryIdentifiers[0].identifier : books.industryIdentifiers[1].identifier;
    // let isbn13 = (books.industryIdentifiers[1].type == "ISBN_13") ? books.industryIdentifiers[1].identifier : books.industryIdentifiers[0].identifier;

    let template = `
    <div class="inner drop-card">
        <div class="description">
        <h3>${books.volumeInfo.title}</h3>
        <ul>
            `
    if (books.volumeInfo.publishedDate)
        template += `<li>Date published: ${books.volumeInfo.publishedDate}</li>`
    
    if (books.volumeInfo.publisher) 
        template += `<li>Publisher: ${books.volumeInfo.publisher}</li>`
    
    
    if (books.volumeInfo.authors) {
        let author = `<li>Author: ${books.volumeInfo.authors[0]}`;
        if (books.volumeInfo.authors.length > 1) {
            for (let i = 1; i < books.volumeInfo.authors.length; i++) {
                author += `, ${books.volumeInfo.authors[i]}`;
            }
        }
        author += "</li>"
        template += author;
    }

    if (books.volumeInfo.categories) {
        let category = `<li>Categories: ${books.volumeInfo.categories[0]}`;
        if (books.volumeInfo.categories.length > 1) {
            for (let i = 1; i < books.volumeInfo.categories.length; i++) {
                category += `, ${books.volumeInfo.categories[i]}`;
            }
        }
        category += "</li>"
        template += category;
    }
    
    let closing = `</ul></div></div>`

    $(".content-wrapper").append(template + closing);
}
