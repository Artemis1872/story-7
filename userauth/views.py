from .forms import CreateUserForm
from django.shortcuts import render, redirect
from django.contrib.auth import logout
from django.contrib.auth.forms import AuthenticationForm


# Create your views here.
def register(request):
    context = {
        'auth': True
    }
    if request.method == 'POST':
        form = CreateUserForm(request.POST)
        context['form'] = form
        if form.is_valid():
            form.save()
            return redirect('userauth:login')

    else:
        context['form'] = CreateUserForm()

    return render(request, 'userauth-register.html', context=context)

def index(request):
    return render(request, 'userauth-secret.html')


def user_logout(request):
    logout(request)
    return redirect('userauth:login')
