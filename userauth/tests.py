from django.test import TestCase
from django.contrib.auth import get_user
from django.contrib.auth.forms import UserCreationForm

# Create your tests here.
class AuthTest(TestCase):
    """
    Test for authentication
    """

    def test_register_account(self):
        register_data = {
            "username": "test_dummy_different_pass",
            "password1": "abc5dasar",
            "password2": "abc5dasar"
        }

        register_form = UserCreationForm(data=register_data)
        self.assertTrue(register_form.is_valid())

    def test_register_password_didnt_match(self):
        register_data = {
            "username": "test_dummy_different_pass",
            "password1": "abc5dasar",
            "password2": "abc5dodol"
        }

        register_form = UserCreationForm(data=register_data)
        self.assertFalse(register_form.is_valid())

    def test_register_common_password(self):
        register_data = {
            "username": "test_dummy_too_common",
            "password1": "asdf12345",
            "password2": "asdf12345"
        }

        register_form = UserCreationForm(data=register_data)
        self.assertFalse(register_form.is_valid())

    def test_success_register_login_logout(self):
        register_data = {
            "username": "test_dummy",
            "password1": "abc5dasar",
            "password2": "abc5dasar"
        }

        register_form = UserCreationForm(data=register_data)
        self.assertTrue(register_form.is_valid())
        register_form.save()

        # Login user
        login_data = {
            "username": "test_dummy",
            "password": "abc5dasar"
        }
        response = self.client.post('/login', data=login_data)
        self.assertEqual(response.status_code, 302)

        # Check auth status after login
        user = get_user(self.client)
        self.assertIsNotNone(user)

        # Logout user
        response = self.client.get('/logout')
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, '/login')

        # Check auth status after logout
        user = get_user(self.client)
        self.assertFalse(user.is_authenticated)
