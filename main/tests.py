from django.test import TestCase

# Create your tests here.
class MainTest(TestCase):
    def test_index_content(self):
        """
        Test for contents inside index
        """
        response = self.client.post('/carousel', follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "Recent Activity")
        self.assertContains(response, "Organizations/Comitee Experiences")
        self.assertContains(response, "Achievement")
        self.assertContains(response, "Contact me!")
