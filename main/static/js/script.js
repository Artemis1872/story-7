$('.accord-item').click((e) => {
    e.preventDefault();

    let $this = $(e.target);

    if ($this.next().hasClass('show')) {
        $this.next().removeClass('show');
        $this.next().slideUp(350);
        $(".expand > i").removeClass("fa-minus").addClass("fa-plus");
    } else {

        $(".expand > i").removeClass("fa-minus").addClass("fa-plus");
        $($this).find(".expand > i").removeClass("fa-plus").addClass("fa-minus");
        $("a").removeClass("active");

        $this.parent().parent().find('li .inner').removeClass('show');
        $this.parent().parent().find('li .inner').slideUp(350);
        $this.next().toggleClass('show');
        $this.next().slideToggle(350);
    }
});

$('.accord-item>a').click((e) => {
    e.preventDefault()
    e.stopPropagation()
    $(e.target).parent().click();
});

$('iframe').prop('width','100%');

$('.fa-chevron-up').click((e) => {
    e.preventDefault();
    e.stopPropagation();

    const $this = $(e.target).parent().parent().parent();
    if ($this.not(':first-child'))
        $this.prev().before($this);
});

$('.fa-chevron-down').click((e) => {
    e.preventDefault();
    e.stopPropagation();

    const $this = $(e.target).parent().parent().parent();
    if ($this.not(':last-child'))
        $this.next().after($this);
});
