from django.test import TestCase

# Create your tests here.
class v8Test(TestCase):
    def test_v8_content(self):
        """
        Test for contents inside v8
        """
        response = self.client.get('/v8', follow=True)
        self.assertEqual(response.status_code, 200)

    def test_v8_api(self):
        """
        Test for Google Books API.
        """

        response = self.client.get('/v8/api', data={'q': 'ayam'}, follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'ayam')
