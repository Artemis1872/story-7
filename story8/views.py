from django.core import serializers
from django.shortcuts import render
from django.test.client import Client
from django.http import JsonResponse
import urllib3
import json

# Create your views here.
def index(request):
    return render(request, "story8-index.html")

def api(request):
    query = dict(request.GET)
    if (q := query.get('q')):
        query['q'] = q[0]
    if (q := query.get('startIndex')):
        query['startIndex'] = q[0]
        
    response = urllib3.PoolManager().request(
        'GET',
        'https://www.googleapis.com/books/v1/volumes',
        fields=query
    )
    context = json.loads(response.data.decode('utf-8'))
    return JsonResponse(context)
